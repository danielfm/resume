.PHONY: pdf img clean

pdf: img
	podman run --rm -i -u root -v `pwd`:/source:Z danielfm/resume

img:
	podman build -t danielfm/resume .

clean:
	rm -f *.{log,out,pdf,aux}
